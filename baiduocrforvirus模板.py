# coding:utf-8

from aip import AipOcr
import os
import time

""" 你的 APPID AK SK """
# 这里修改为你申请的应用的才能用
APP_ID = '你的appid'
API_KEY = '你的apikey'
SECRET_KEY = '你的secretkey'

client = AipOcr(APP_ID, API_KEY, SECRET_KEY)

# 百度api通用识别

""" 读取图片 """
def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()


# data_folder = "company"   # 企业版
data_folder = "home"   # 家庭版
if os.path.exists(data_folder + ".txt"):
    os.remove(data_folder + ".txt")
filenames = os.listdir(data_folder)
for i in range(len(filenames)):
    filename = str(i + 1) + ".jpg"
    print(filename)
    image = get_file_content(data_folder + "\\" + filename)

    """ 调用通用文字识别, 图片参数为本地图片 """
    res = client.basicGeneral(image)

    print("唯一的log id，用于问题定位:", res.get("log_id", ""))
    print("识别结果数:", res.get("words_result_num", ""))
    print("识别结果:")
    words_result = res.get("words_result")
    content_results = []
    for per_result in words_result:
        content_results.append(per_result.get("words", ""))
    all_contents = "\n".join(content_results)
    print(all_contents)
    with open(data_folder + ".txt", "a", encoding = "utf-8") as f:
        f.writelines(all_contents + "\n")
    print()

    time.sleep(1)






